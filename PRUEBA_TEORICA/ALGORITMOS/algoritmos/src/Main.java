import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    //repite solicitud de entrada y salida
    while (true) {
      System.out.println("Ingrese la cadena de letras separadas por '|':");
      String entrada = scanner.nextLine();

      String[] subcadenas = entrada.split("\\|");

      //verifica que la subcadena sea de 3 o 4
      if (subcadenas.length < 3 || subcadenas.length > 4) {
        System.out.println("La cadena no contiene 3 o 4 subcadenas separadas por '|'. Número de subcadenas = " + subcadenas.length);
        continue;
      }

      StringBuilder resultado = new StringBuilder();

      boolean esValido = true;

      for (String subcadena : subcadenas) {
        if (!validarSubcadena(subcadena)) {
          System.out.println("La subcadena '" + subcadena + "' no cumple con el formato requerido.");
          esValido = false;
          break;
        }

        resultado.append(procesarSubcadena(subcadena)).append("|");
      }

      if (!esValido) {
        continue;
      }

      resultado.deleteCharAt(resultado.length() - 1); // Eliminar el último '|'

      System.out.println("Cadena Resultante: " + resultado);
    }
  }

  //valida cantidad de carcteres
  private static boolean validarSubcadena(String subcadena) {
    if (subcadena.length() != 5) {
      return false;
    }

    for (char c : subcadena.toCharArray()) {
      if (!Character.isLetter(c) || subcadena.chars().filter(ch -> ch == c).count() > 2) {
        return false;
      }
    }

    return true;
  }

  //comparacion de la cadena
  private static String procesarSubcadena(String subcadena) {
    String subcadenaOpuesta = new StringBuilder(subcadena).reverse().toString();
    StringBuilder resultado = new StringBuilder();

    for (int i = 0; i < subcadena.length(); i++) {
      char c = subcadena.charAt(i);
      char cOpuesto = subcadenaOpuesta.charAt(i);

      int conteoC = contarRepeticiones(subcadena, c);
      int conteoCOpuesto = contarRepeticiones(subcadenaOpuesta, cOpuesto);

      if (conteoC > conteoCOpuesto) {
        resultado.append(subcadena.charAt(i));
      } else if (conteoC < conteoCOpuesto) {
        resultado.append(cOpuesto);
      } else {
        int indiceMaximo = Math.max(subcadena.lastIndexOf(c), subcadenaOpuesta.lastIndexOf(cOpuesto));
        if (indiceMaximo < 4) {
          resultado.append(subcadena.charAt(i));
        } else {
          resultado.append(cOpuesto);
        }
      }
    }

    return resultado.toString();
  }

  private static int contarRepeticiones(String str, char objetivo) {
    int conteo = 0;
    for (char c : str.toCharArray()) {
      if (c == objetivo) {
        conteo++;
      }
    }
    return conteo;
  }
}