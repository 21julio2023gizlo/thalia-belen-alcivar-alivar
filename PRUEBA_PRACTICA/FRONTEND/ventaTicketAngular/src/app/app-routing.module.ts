import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { EventListComponent } from './components/event-list/event-list.component';
import { FormEventComponent } from './components/form-event/form-event.component';

const routes: Routes = [
  { path: "", pathMatch: 'full', redirectTo: 'login' },
  { path: "login", component: LoginComponent },
  { path: "event", component: EventListComponent },
  { path: "new-event", component: FormEventComponent },
  { path: 'update-event/:idevent', component: FormEventComponent },

  { path: "**", redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
