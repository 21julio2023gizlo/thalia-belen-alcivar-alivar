import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from 'src/app/services/events.service';
import { Event } from 'src/app/interfaces/event.interface'
import Swal from 'sweetalert2';



@Component({
  selector: 'app-form-event',
  templateUrl: './form-event.component.html',
  styleUrls: ['./form-event.component.css']
})
export class FormEventComponent implements OnInit{
  formEvent: FormGroup
  type: string = 'Register';
  constructor(
    private eventsService: EventsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ){

    this.formEvent= new FormGroup ({

      nameEvent: new FormControl('', [
        Validators.required, 
      ]),
      dateEvent: new FormControl('', [
        Validators.required
      ]),
      ticketNumber: new FormControl('', [
        Validators.required
      ]),
      price: new FormControl('', [
        Validators.required,
      ])

    }, []);
  }

  async getDataForm() : Promise<void>{
    if(this.formEvent.valid) { } 
    else {
      Swal.fire(
      'Informacion!',
      'El formulario no esta bien relleno',
      'info');
    }
    
    let newEvent = this.formEvent.value; 
    if(newEvent.eventId > 0) {
      let response = await this.eventsService.update(newEvent);
      if(response.eventId) {
        
        Swal.fire(
          'OK!',
          'Updated event',
          'success')
          .then((result) => {
            this.router.navigate(['/event']);
        });          
      } 
      else {
        Swal.fire(
          'Error!',
          response.error,
          'error')
          .then((result) => {
            this.router.navigate(['/new-event']);
        });
      }  
    } 
    else {
      let response = await this.eventsService.create(newEvent)
      if(response.eventId) {
        Swal.fire(
          'OK!',
          'event created!',
          'success')
          .then((result) => {
            this.router.navigate(['/event']);
        });
      } 
      else {
        Swal.fire(
          'Error!',
          'Error with new event',
          'error')
          .then((result) => {
            this.router.navigate(['/new-event']);
        });
      }  
    }

  }

  ngOnInit(): void {  
    this.activatedRoute.params.subscribe(async(params: any) => {
      let id: number = parseInt(params.idevent);
      if(id){
        this.type = 'Update'
        const response = await this.eventsService.getById(id)
        const event: Event = response
        this.formEvent= new FormGroup({
          nameEvent: new FormControl(event?.nameEvent, []),
          dateEvent: new FormControl(event?.dateEvent, []),
          ticketNumber: new FormControl(event?.ticketNumber, []),
          price: new FormControl(event?.price, []),
          eventId: new FormControl(event?.eventId, [])
        }, [])
      }
    })
  }

  checkControl(pControlName: string, pError: string): boolean{
    if(this.formEvent.get(pControlName)?.hasError(pError) && this.formEvent.get(pControlName)?.touched){
      return true;
    } 
    else {
      return false;
    }
  }

}
