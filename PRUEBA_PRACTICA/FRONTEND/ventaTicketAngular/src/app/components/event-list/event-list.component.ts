import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventsService } from 'src/app/services/events.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit{
  myEvent: Event | any;
  constructor(
    private eventsService: EventsService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async(params: any) => {
      let eventId: number = parseInt(params.idEvent)
      let response = await this.eventsService.getAll();
      console.log(response)
      if(response.error){
        Swal.fire(response.error, '', 'error');
      }   
      this.myEvent = response;
      console.log(this.myEvent);
    })
  }

  deleteEvent(pId: number | undefined): void {
    Swal.fire({
      title: "Do you want to delete event ?",
      showDenyButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) { 
        if(pId !== undefined) { 
          this.eventsService.delete(pId).then(response => {
          console.log(response)
          
            Swal.fire(
            'OK!',
            'Event deleted',
            'success')
            .then((result) => {
              this.eventsService.getAll()
            });
            
          })
          .catch(err => console.log(err))
        }       
      }
    })      
  } 

}
