import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit{
  formEvent: FormGroup
  type: string = 'Register';
  constructor(
    private usersService: UsersService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ){

    this.formEvent= new FormGroup ({
      email: new FormControl('', [
        
      ]),
      password: new FormControl('', [
      
      ])
    }, []);
  }

  async getDataForm() : Promise<void>{
    if(this.formEvent.valid) { } 
    else {
      Swal.fire(
      'Informacion!',
      'El formulario no esta bien relleno',
      'info');
    }
    
    let login = this.formEvent.value; 
    console.log(login)
      let response = await this.usersService.login(login);
      console.log(response)
      if(response.userId) {
        if (response.rolId === 1) {
          // Redirigir al componente para administradores
          this.router.navigate(['/event']);
        } else {
          // Redirigir al componente para usuarios normales
          this.router.navigate(['/usuario']);
        }
    
      } 
      else {
        Swal.fire(
          'Error!',
          response.error,
          'error')
          .then((result) => {
            this.router.navigate(['/login']);
        });
        
      }  
    
    }
   

  ngOnInit(): void {  
    
  }
}
