import { Rol } from "./rol.interface";

export interface User {
  userId?: number;
  name: string;
  lastName: string;
  email: string;
  password: string;
  rol: Rol;
}
