export interface Event {
  eventId?: number;
  nameEvent: string;
  dateEvent: Date;
  ticketNumber: number;
  price: number;
}
