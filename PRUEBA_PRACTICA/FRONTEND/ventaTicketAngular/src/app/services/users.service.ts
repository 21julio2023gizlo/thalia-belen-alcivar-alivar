import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';
import { Injectable } from '@angular/core';
import { Login } from '../interfaces/login.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  baseUrl: string = 'http://localhost:8080/ventaTicket/api/users/';
  constructor(private httpClient: HttpClient) { }

  login(pLogin: Login): Promise<any> {  
    return lastValueFrom(this.httpClient.post<Login>(this.baseUrl+'login', pLogin))
  }

}
