import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RolsService {
  baseUrl: string = 'http://localhost:8080/ventaTicket/api/rol/';

  constructor(private httpClient: HttpClient) { }

  getById(pId: number): Promise<any> {
    return lastValueFrom(this.httpClient.get<any>(`${this.baseUrl}${pId}`))
  }
}
