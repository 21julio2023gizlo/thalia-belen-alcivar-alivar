import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';
import { Injectable } from '@angular/core';
import { Event} from '../interfaces/event.interface';


@Injectable({
  providedIn: 'root'
})
export class EventsService {
  baseUrl: string = 'http://localhost:8080/ventaTicket/api/events/';

  constructor(private httpClient: HttpClient) { }
    create(pEvent: Event): Promise<any> {  
      return lastValueFrom(this.httpClient.post<Event>(this.baseUrl+'save', pEvent))
    }
  
    getAll(): Promise<any> {
      return lastValueFrom(this.httpClient.get<any>(`${this.baseUrl+'all'}`))
  
    }
  
    getById(pId: number): Promise<any> {
      return lastValueFrom(this.httpClient.get<any>(`${this.baseUrl}${pId}`))
    }
  
    update(pEvent: Event): Promise<any> {
      return lastValueFrom(this.httpClient.put<any>(`${this.baseUrl+'update'}`, pEvent))
    }
  
    delete(pId: number): Promise<any> {
      return lastValueFrom(this.httpClient.delete<any>(`${this.baseUrl+'delete/'}${pId}`))
    }
 
}
