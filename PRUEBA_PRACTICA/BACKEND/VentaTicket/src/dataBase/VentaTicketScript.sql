create database ventaTicket;

create table if not exists ventaTicket.cargo(
cargo_id int not null auto_increment,           
cargo varchar(50) not null,
primary key (cargo_id)
)engine= InnoDB default character set= utf8mb3;

create table if not exists ventaTicket.usuario(
usuario_id int not null auto_increment,           
nombre varchar(50) not null,
apellido varchar(50) not null,
fechaNacimiento TIMESTAMP not null,
correo varchar(50) not null,
contrasena varchar(200) not null,
cargo_id int not null,           
primary key (usuario_id),
foreign key(cargo_id) references ventaTicket.cargo(cargo_id) on delete cascade on update cascade
)engine= InnoDB default character set= utf8mb3;


create table if not exists ventaTicket.evento(
evento_id int not null auto_increment,           
nombreEvento varchar(50) not null,
fechaEvento TIMESTAMP not null,
numeroTickets int not null,
valor decimal(10, 2) not null,
primary key (evento_id)
)engine= InnoDB default character set= utf8mb3;

create table if not exists ventaTicket.compras (
    compras_id INT PRIMARY KEY AUTO_INCREMENT,
    cantidadTickets INT NOT NULL,
    valorPago DECIMAL(10, 2) NOT NULL,
    codigoCompra VARCHAR(10) UNIQUE NOT NULL,
    fechaCompra DATE NOT NULL,
    fechaRetiro DATE NOT NULL,
    numeroContacto VARCHAR(20) NOT NULL,
    evento_id INT NOT NULL,
    usuario_id INT NOT NULL,
    FOREIGN KEY (evento_id) REFERENCES ventaTicket.evento(evento_id),
    FOREIGN KEY (usuario_id) REFERENCES ventaTicket.usuario(usuario_id)
);
insert into  ventaticket.cargo (cargo)
values ('Administrador'),
 ('Comprador');
 insert into  ventaticket.usuario (nombre,apellido,fechaNacimiento,correo,contrasena,cargo_id)
 values  ('Thalia','Alcivar','2022-03-01 00:00:00','admin@test.com','1234',1),
  ('Belen','Zambrano','2022-03-01 00:00:00','comprador@test.com','5678',1);
 
select * from ventaticket.cargo;
select * from ventaticket.usuario;

