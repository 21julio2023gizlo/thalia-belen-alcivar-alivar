package com.ticket.VentaTicket.persistence.crud;

import com.ticket.VentaTicket.persistence.entity.Cargo;
import org.springframework.data.repository.CrudRepository;

public interface CargoCrudRepository extends CrudRepository<Cargo, Integer> {
}
