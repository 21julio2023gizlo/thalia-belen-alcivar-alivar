package com.ticket.VentaTicket.persistence.mapper;

import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.persistence.entity.Evento;
import com.ticket.VentaTicket.persistence.entity.Usuario;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")

public interface EventMapper {
  @Mappings({
    @Mapping(source = "idEvento", target = "eventId"),
    @Mapping(source = "nombreEvento", target = "nameEvent"),
    @Mapping(source = "fechaEvento", target = "dateEvent"),
    @Mapping(source = "numeroTickets", target = "ticketNumber"),
    @Mapping(source = "valor", target = "price"),


  })
  Event toEvent(Evento evento);
  List<Event> toEvents(List<Evento>eventos);
  @InheritInverseConfiguration
  Evento toEvento(Event event);
}
