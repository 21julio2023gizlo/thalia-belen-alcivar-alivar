package com.ticket.VentaTicket.persistence.crud;

import com.ticket.VentaTicket.persistence.entity.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraCrudRepository extends CrudRepository<Compra, Integer> {
}
