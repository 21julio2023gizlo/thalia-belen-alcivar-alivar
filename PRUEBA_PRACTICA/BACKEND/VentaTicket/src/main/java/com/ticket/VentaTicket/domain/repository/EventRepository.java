package com.ticket.VentaTicket.domain.repository;

import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.User;

import java.util.List;
import java.util.Optional;

public interface EventRepository {
  List<Event> getAll();
  Optional<Event> getEvent(int eventId);
  Event save(Event event);
  void delete(int eventId);
}
