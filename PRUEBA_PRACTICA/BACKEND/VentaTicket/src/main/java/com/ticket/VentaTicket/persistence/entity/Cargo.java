package com.ticket.VentaTicket.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "")
public class Cargo {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cargo_id")
  private Integer idCargo;
  private String cargo;


  public Integer getIdCargo() {
    return idCargo;
  }

  public void setIdCargo(Integer idCargo) {
    this.idCargo = idCargo;
  }

  public String getCargo() {
    return cargo;
  }

  public void setCargo(String cargo) {
    this.cargo = cargo;
  }
}
