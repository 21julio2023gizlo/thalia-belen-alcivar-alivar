package com.ticket.VentaTicket.domain.service;

import com.ticket.VentaTicket.domain.Rol;
import com.ticket.VentaTicket.domain.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RolService {
  @Autowired
  private RolRepository rolRepository;

  public Optional<Rol> getRol(int rold){
    Optional<Rol> rolIdOut = rolRepository.getRol(rold);
    return (Optional<Rol>) rolIdOut;
  }
}
