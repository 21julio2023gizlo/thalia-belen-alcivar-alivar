package com.ticket.VentaTicket.domain;

import java.io.StringReader;
import java.util.Date;

public class BuyTicket {
  private int buyTicketId;
  private int ticketAmount;
  private double paymentValue;
  private String purchaseCode;
  private Date dateOfPurchase;
  private Date ticketWithdrawal;
  private String phoneNumber;
  private int userId;
  private int eventId;

  public int getBuyTicketId() {
    return buyTicketId;
  }

  public void setBuyTicketId(int buyTicketId) {
    this.buyTicketId = buyTicketId;
  }

  public int getTicketAmount() {
    return ticketAmount;
  }

  public void setTicketAmount(int ticketAmount) {
    this.ticketAmount = ticketAmount;
  }

  public double getPaymentValue() {
    return paymentValue;
  }

  public void setPaymentValue(double paymentValue) {
    this.paymentValue = paymentValue;
  }

  public String getPurchaseCode() {
    return purchaseCode;
  }

  public void setPurchaseCode(String purchaseCode) {
    this.purchaseCode = purchaseCode;
  }

  public Date getDateOfPurchase() {
    return dateOfPurchase;
  }

  public void setDateOfPurchase(Date dateOfPurchase) {
    this.dateOfPurchase = dateOfPurchase;
  }

  public Date getTicketWithdrawal() {
    return ticketWithdrawal;
  }

  public void setTicketWithdrawal(Date ticketWithdrawal) {
    this.ticketWithdrawal = ticketWithdrawal;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getEventId() {
    return eventId;
  }

  public void setEventId(int eventId) {
    this.eventId = eventId;
  }
}
