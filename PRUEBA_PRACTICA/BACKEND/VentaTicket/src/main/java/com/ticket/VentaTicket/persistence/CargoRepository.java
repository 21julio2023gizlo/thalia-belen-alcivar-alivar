package com.ticket.VentaTicket.persistence;

import com.ticket.VentaTicket.domain.Rol;
import com.ticket.VentaTicket.domain.repository.RolRepository;
import com.ticket.VentaTicket.persistence.crud.CargoCrudRepository;
import com.ticket.VentaTicket.persistence.mapper.RolMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public class CargoRepository implements RolRepository {
  @Autowired
  private CargoCrudRepository cargoCrudRepository;
  @Autowired
  private RolMapper rolMapper;
  @Override
  public Optional<Rol> getRol(int rolId) {
    return cargoCrudRepository.findById(rolId).map(cargo -> rolMapper.toRol (cargo));
  }
}
