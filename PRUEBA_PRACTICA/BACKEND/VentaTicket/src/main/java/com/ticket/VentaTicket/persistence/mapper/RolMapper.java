package com.ticket.VentaTicket.persistence.mapper;

import com.ticket.VentaTicket.domain.Rol;
import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.persistence.entity.Cargo;
import com.ticket.VentaTicket.persistence.entity.Usuario;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")

public interface RolMapper {
  @Mappings({
    @Mapping(source = "idCargo", target = "rolId"),
    @Mapping(source = "cargo", target = "rol"),

  })
  Rol toRol(Cargo cargo);
  List<Rol> toRols(List<Cargo>cargos);
  @InheritInverseConfiguration
  Cargo toCargo(Rol rol);
}
