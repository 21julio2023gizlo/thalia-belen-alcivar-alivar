package com.ticket.VentaTicket.domain;

import javax.xml.crypto.Data;
import java.text.DateFormat;
import java.util.Date;

public class User {
  private int userId;
  private String name;
  private String lastName;
  private Date birthday;
  private String email;
  private String password;
  private int rolId;

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getRolId() {
    return rolId;
  }

  public void setRolId(int rolId) {
    this.rolId = rolId;
  }
}
