package com.ticket.VentaTicket.domain;

import java.util.Date;

public class Event {
  private int eventId;
  private String nameEvent;
  private Date dateEvent;
  private int ticketNumber;
  private double price;

  public int getEventId() {
    return eventId;
  }

  public void setEventId(int eventId) {
    this.eventId = eventId;
  }

  public String getNameEvent() {
    return nameEvent;
  }

  public void setNameEvent(String nameEvent) {
    this.nameEvent = nameEvent;
  }

  public Date getDateEvent() {
    return dateEvent;
  }

  public void setDateEvent(Date dateEvent) {
    this.dateEvent = dateEvent;
  }

  public int getTicketNumber() {
    return ticketNumber;
  }

  public void setTicketNumber(int ticketNumber) {
    this.ticketNumber = ticketNumber;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}
