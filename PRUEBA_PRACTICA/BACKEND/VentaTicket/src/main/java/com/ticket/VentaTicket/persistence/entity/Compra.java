package com.ticket.VentaTicket.persistence.entity;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.util.Date;

@Entity
@Table(name = "compra")
public class Compra {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "compra_id")
  private Integer idCompra;
  private Integer cantidadTickets;
  private  Double valorPago;
  private String codigoCompra;
  private Date fechaCompra;
  private Date fechaRetiro;
  private String numeroContacto;
  @Column(name = "usuario_id")
  private Integer idUsuario;
  @Column(name = "evento_id")
  private Integer idEvento;

  public Integer getIdCompra() {
    return idCompra;
  }

  public void setIdCompra(Integer idCompra) {
    this.idCompra = idCompra;
  }

  public Integer getCantidadTickets() {
    return cantidadTickets;
  }

  public void setCantidadTickets(Integer cantidadTickets) {
    this.cantidadTickets = cantidadTickets;
  }



  public String getCodigoCompra() {
    return codigoCompra;
  }

  public void setCodigoCompra(String codigoCompra) {
    this.codigoCompra = codigoCompra;
  }

  public Double getValorPago() {
    return valorPago;
  }

  public void setValorPago(Double valorPago) {
    this.valorPago = valorPago;
  }

  public Date getFechaCompra() {
    return fechaCompra;
  }

  public void setFechaCompra(Date fechaCompra) {
    this.fechaCompra = fechaCompra;
  }

  public Date getFechaRetiro() {
    return fechaRetiro;
  }

  public void setFechaRetiro(Date fechaRetiro) {
    this.fechaRetiro = fechaRetiro;
  }

  public String getNumeroContacto() {
    return numeroContacto;
  }

  public void setNumeroContacto(String numeroContacto) {
    this.numeroContacto = numeroContacto;
  }

  public Integer getIdUsuario() {
    return idUsuario;
  }

  public void setIdUsuario(Integer idUsuario) {
    this.idUsuario = idUsuario;
  }

  public Integer getIdEvento() {
    return idEvento;
  }

  public void setIdEvento(Integer idEvento) {
    this.idEvento = idEvento;
  }
}
