package com.ticket.VentaTicket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaTicketApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaTicketApplication.class, args);
	}

}
