package com.ticket.VentaTicket.domain.service;

import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;
  public Optional<User> getLogin(String correo, String contrasena){
    Optional<User> userIdOut = userRepository.getLogin(correo, contrasena);
    return (Optional<User>) userIdOut;
  }

}
