package com.ticket.VentaTicket.web.controller;

import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.Login;
import com.ticket.VentaTicket.domain.Rol;
import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/users")

public class UserController {
  @Autowired
  private UserService userService;
  @PostMapping("/login")
  public ResponseEntity<User> login (@RequestBody Login login){
    Optional<User> userOptional = userService.getLogin(login.getEmail(), login.getPass());
    if (userOptional.isPresent()) {
      User user= userOptional.get();
      return new ResponseEntity<>(user, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
