package com.ticket.VentaTicket.domain.service;

import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EventService {
  @Autowired
  private EventRepository eventRepository;

  public List<Event> getAll(){
    List<Event> eventGetAll = eventRepository.getAll();
    return (List<Event>) eventGetAll;
  }

  public Optional<Event> getEvent(int eventId){
    Optional<Event> eventIdOut = eventRepository.getEvent(eventId);
    return (Optional<Event>) eventIdOut;
  }

  public Event save(Event event){
    Event eventSaveOut = eventRepository.save(event);
    return eventSaveOut;
  }

  public boolean delete(int eventId){
    return getEvent(eventId).map(event -> {
      eventRepository.delete(eventId);
      return true;
    }).orElse(false);
  }
}
