package com.ticket.VentaTicket.persistence.crud;

import com.ticket.VentaTicket.persistence.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioCrudRepository extends CrudRepository<Usuario, Integer> {
  Optional <Usuario> findByCorreoAndContrasena(String correo, String contrasena);
}
