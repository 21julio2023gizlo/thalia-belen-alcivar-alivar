package com.ticket.VentaTicket.domain;

public class Rol {
  private int rolId;
  private String rol;

  public int getRolId() {
    return rolId;
  }

  public void setRolId(int rolId) {
    this.rolId = rolId;
  }

  public String getRol() {
    return rol;
  }

  public void setRol(String rol) {
    this.rol = rol;
  }
}
