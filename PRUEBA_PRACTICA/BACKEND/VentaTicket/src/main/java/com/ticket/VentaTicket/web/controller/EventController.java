package com.ticket.VentaTicket.web.controller;

import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/events")

public class EventController {
  @Autowired
  private EventService eventService;
  @GetMapping("/all")
  public ResponseEntity<List<Event>> getAll(){
    return new ResponseEntity<>(eventService.getAll(), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Event> getEventById(@PathVariable("id") int eventId) {
    Optional<Event> eventOptional = eventService.getEvent(eventId);
    if (eventOptional.isPresent()) {
      Event event = eventOptional.get();
      return new ResponseEntity<>(event, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/save")
  public ResponseEntity<Event> save (@RequestBody Event event){
    return new ResponseEntity<>(eventService.save(event), HttpStatus.CREATED);
  }

  @PutMapping("/update")
  public ResponseEntity<Event> update (@RequestBody Event event){
    return new ResponseEntity<>(eventService.save(event), HttpStatus.CREATED);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity delete(@PathVariable("id") int eventId){
    if(eventService.delete(eventId)){
      return new ResponseEntity<>(HttpStatus.OK);
    }else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
