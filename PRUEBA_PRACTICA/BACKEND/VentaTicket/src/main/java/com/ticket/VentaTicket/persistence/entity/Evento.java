package com.ticket.VentaTicket.persistence.entity;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.util.Date;

@Entity
@Table(name = "evento")
public class Evento {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "evento_id")
  private Integer idEvento;
  private String nombreEvento;
  private Date fechaEvento;
  private Integer numeroTickets;
  private Double valor;

  public Integer getIdEvento() {
    return idEvento;
  }

  public void setIdEvento(Integer idEvento) {
    this.idEvento = idEvento;
  }

  public String getNombreEvento() {
    return nombreEvento;
  }

  public void setNombreEvento(String nombreEvento) {
    this.nombreEvento = nombreEvento;
  }

  public Date getFechaEvento() {
    return fechaEvento;
  }

  public void setFechaEvento(Date fechaEvento) {
    this.fechaEvento = fechaEvento;
  }

  public Integer getNumeroTickets() {
    return numeroTickets;
  }

  public void setNumeroTickets(Integer numeroTickets) {
    this.numeroTickets = numeroTickets;
  }

  public Double getValor() {
    return valor;
  }

  public void setValor(Double valor) {
    this.valor = valor;
  }
}
