package com.ticket.VentaTicket.domain.repository;

import com.ticket.VentaTicket.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
  List<User> getAll();
  Optional<User> getUser(int userId);
  Optional<User> getLogin(String correo, String contrasena);
  User save(User user);
  void delete(int userId);
}
