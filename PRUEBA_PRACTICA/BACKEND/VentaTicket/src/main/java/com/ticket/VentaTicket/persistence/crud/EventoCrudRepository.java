package com.ticket.VentaTicket.persistence.crud;

import com.ticket.VentaTicket.persistence.entity.Evento;
import org.springframework.data.repository.CrudRepository;

public interface EventoCrudRepository extends CrudRepository<Evento,Integer> {
}
