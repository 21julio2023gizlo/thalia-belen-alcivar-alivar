package com.ticket.VentaTicket.persistence.mapper;

import com.ticket.VentaTicket.domain.BuyTicket;
import com.ticket.VentaTicket.domain.Event;
import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.persistence.entity.Compra;
import com.ticket.VentaTicket.persistence.entity.Evento;
import com.ticket.VentaTicket.persistence.entity.Usuario;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")

public interface BuyTicketMapper {
  @Mappings({
    @Mapping(source = "idCompra", target = "buyTicketId"),
    @Mapping(source = "cantidadTickets", target = "ticketAmount"),
    @Mapping(source = "valorPago", target = "paymentValue"),
    @Mapping(source = "codigoCompra", target = "purchaseCode"),
    @Mapping(source = "fechaCompra", target = "dateOfPurchase"),
    @Mapping(source = "fechaRetiro", target = "ticketWithdrawal"),
    @Mapping(source = "numeroContacto", target = "phoneNumber"),
    @Mapping(source = "idUsuario", target = "userId"),
    @Mapping(source = "idEvento", target = "eventId"),

  })
  BuyTicket toBuyTicket(Compra compra);
  List<BuyTicket> toBuyTickets(List<Evento>eventos);
  @InheritInverseConfiguration
  Compra toCompra(BuyTicket buyTicket);
}
