package com.ticket.VentaTicket.persistence.mapper;

import com.ticket.VentaTicket.domain.User;
import com.ticket.VentaTicket.persistence.entity.Usuario;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")

public interface UserMapper {
  @Mappings({
    @Mapping(source = "idUsuario", target = "userId"),
    @Mapping(source = "nombre", target = "name"),
    @Mapping(source = "apellido", target = "lastName"),
    @Mapping(source = "fechaNacimiento", target = "birthday"),
    @Mapping(source = "correo", target = "email"),
    @Mapping(source = "contrasena", target = "password"),
    @Mapping(source = "idCargo", target = "rolId"),
  })
  User toUser(Usuario usuario);
  List<User> toUsers(List<Usuario>usuarios);
  @InheritInverseConfiguration
  Usuario toUsuario(User user);
}
