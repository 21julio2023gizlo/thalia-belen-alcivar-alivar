package com.ticket.VentaTicket.web.controller;

import com.ticket.VentaTicket.domain.Rol;
import com.ticket.VentaTicket.domain.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/rol")

public class RolController {

  @Autowired
  private RolService rolService;

  @GetMapping("/{id}")
  public ResponseEntity<Rol> getRolById(@PathVariable("id") int rolId) {
    Optional<Rol> rolOptional = rolService.getRol(rolId);
    if (rolOptional.isPresent()) {
      Rol rol= rolOptional.get();
      return new ResponseEntity<>(rol, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
