package com.ticket.VentaTicket.domain.repository;

import com.ticket.VentaTicket.domain.Rol;

import java.util.Optional;

public interface RolRepository {
  Optional<Rol> getRol(int rolId);
}
